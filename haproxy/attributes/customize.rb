###
# This is the place to override the haproxy cookbook's default attributes.
#
# Do not edit THIS file directly. Instead, create
# "haproxy/attributes/customize.rb" in your cookbook repository and
# put the overrides in YOUR customize.rb file.
###

# The following shows how to override the HA-Proxy stats url and user:
#
#normal[:haproxy][:stats_url] = '/haproxy?stats'
#normal[:haproxy][:stats_user] = 'opsworks'
normal[:haproxy][:crt_dir] = '/etc/haproxy'
normal[:haproxy][:crt_file] = '/etc/haproxy/ssl.pem'
normal[:haproxy][:ca_file] = '/etc/haproxy/client.pem'
normal[:haproxy][:server_domain] = 'api-usw2-main.rewbase.com'
if node['hostname'] == "lb2"
  normal[:haproxy][:server_domain] = 'api-usw2-sec.rewbase.com'
end
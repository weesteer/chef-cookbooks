# Create /etc/httpd/ssl directory on chef client
directory "#{node[:haproxy][:crt_dir]}/ssl" do
        owner 'root'
        group 'root'
        action :create
        recursive true
        mode 0755
end
remote_directory "#{node[:haproxy][:crt_dir]}/ssl" do
        source "certificates"
        files_owner "root"
        files_group "root"
        files_mode 00644
        owner "root"
        group "root"
        mode 0755
end
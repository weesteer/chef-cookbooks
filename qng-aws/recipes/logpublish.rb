#
# Cookbook Name:: qng-aws
# Recipe:: logpublish
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
# Use the power of ruby:
%w{/var/log/tomcat7/rotated /var/log/tomcat7/uploaded /var/log/httpd/rotated /var/log/httpd/uploaded}.each do |dir|
  directory dir do
    owner 'root'
    group 'root'
    mode '0755'
    action :create
  end
end

remote_directory '/etc/logrotate.elasticbeanstalk.hourly' do
  source 'logrotate.elasticbeanstalk.hourly'
  owner 'root'
  group 'root'
  mode '0755'
  files_mode '0644'
  purge false
  action :create
end

remote_directory '/etc/cron.hourly' do
  source 'cron.hourly'
  owner 'root'
  group 'root'
  mode '0755'
  files_mode '0755'
  purge false
  action :create
end

remote_directory '/usr/bin' do
  source 'sh'
  owner 'root'
  group 'root'
  mode '0555'
  files_mode '0755'
  purge false
  action :create
end

remote_directory '/etc/cron.d' do
  source 'cron.d'
  owner 'root'
  group 'root'
  mode '0755'
  files_mode '0644'
  purge false
  action :create
end

template '/usr/bin/logpublish.sh' do
  source 'logpublish.sh.erb'
  owner 'root'
  group 'root'
  mode '0755'
end

template '/usr/bin/tomcatlive.sh' do
  source 'tomcatlive.sh.erb'
  owner 'root'
  group 'root'
  mode '0755'
end
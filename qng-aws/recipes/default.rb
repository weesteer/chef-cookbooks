#
# Cookbook Name:: qng-aws
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
ssl_path = node['custom_ssl_path1']
if node['hostname'] == "lb2"
  ssl_path = node['custom_ssl_path2']
end

s3_file node['qng-aws']['ssl_local_path'] do
  bucket node['custom_data_bucket']
  remote_path ssl_path
  mode 0440
  aws_access_key_id node['custom_access_key']
  aws_secret_access_key node['custom_secret_key']
end

s3_file "/etc/haproxy/client.pem" do
  bucket node['custom_data_bucket']
  remote_path node['custom_ssl_ca_path']
  mode 0440
  aws_access_key_id node['custom_access_key']
  aws_secret_access_key node['custom_secret_key']
end
#
# Cookbook Name:: qng-aws
# Recipe:: cfsignsetup
#
# Copyright (c) 2016 The Authors, All Rights Reserved.
# Use the power of ruby:
directory '/etc/tomcat7/cfkeys' do
  owner 'tomcat'
  group 'tomcat'
  mode '0770'
  action :create
end
#
# Cookbook Name:: qng-aws
# Recipe:: updateclcert
# Updates the client certs on load-balancers
# Copyright (c) 2016 The Authors, All Rights Reserved.
s3_file "/etc/haproxy/client.pem" do
  bucket node['custom_data_bucket']
  remote_path node['custom_ssl_ca_path']
  mode 0440
  aws_access_key_id node['custom_access_key']
  aws_secret_access_key node['custom_secret_key']
end

service "haproxy" do
  supports :restart => true, :status => true, :reload => true
  action :nothing  # wait for status check
end

execute "echo 'checking if HAProxy is running - if so restart it'" do
  only_if "pgrep haproxy"
  notifies :restart, "service[haproxy]"
end
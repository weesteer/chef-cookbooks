#!/bin/bash
function cleanlogs() {
  if [ -z "$1" ] ; then
    echo log type not specified!
    exit 1
  fi
  tsnow=$(date +%s)
  searchstr=/var/log/$1/uploaded/*
  for filename in $searchstr; do
     if [ $filename == "$searchstr" ] ; then
         break
     fi
     tsfile=$(ls -l --time-style="+%s" ${filename} | cut -d ' ' -f 6)
     age=$(($tsnow-$tsfile))
     #echo file-age of $filename is: $age
     #Delete all logs older than 7 days
     if [ $age -gt 604800 ] ; then
       echo Deleteing old log file: $filename
       rm $filename
     fi
  done
}
cleanlogs tomcat7
cleanlogs httpd